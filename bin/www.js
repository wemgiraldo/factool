#!/usr/bin/env node

/**
 * Module dependencies.
 */

app = require('../app');
debug = require('debug')('factools:server');
http = require('http');
mysql = require('mysql');
moment = require('moment-timezone');
fs = require("fs.extra");  // Oggetto per la lettura filesystem
HigJS = require("../nodeLib/hig.js").HigJS;      // Higeco's Base Functions
CEN_API = require('../nodeLib/cen.js');
FacturacionCL_API = require('../nodeLib/facturacion-cl.js');
HigecoPortalDriver = require('../nodeLib/higeco.js');
models = require('../models');
async = require("async");
json2xls = require('json2xls');
XLSX = require('xlsx');
path = require('path');
nodemailer = require('nodemailer');
CronJob = require('cron').CronJob;
schedule = require('node-schedule');


httpServer = null;
config = null;
cen = null;
facturacion_cl = null;
higeco_driver = null;
transporter = null;

global.appRoot = path.resolve(__dirname.replace('bin', ''));

configPath = path.join(global.appRoot, '/config/config.json')

// mod MATTEO 03/12/2019
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

/**
 *  Configuration
**/

config = HigJS.str.toObj(fs.readFileSync(configPath));       // Lettura configurazione server

if (!checkConfig()) {
  process.exit(1);
}

logger = new HigJS.Logger(config.debug.LOG);

fs.watchFile(configPath, function () {

  logger.log("Reloading config...", "inf");

  config = HigJS.str.toObj(fs.readFileSync(configPath)) || {};

  if (!checkConfig()) {
    process.exit(1);
  }

  initDbConnection();
  initServer();
  if (process.env.NODE_ENV != "development") initMail();
});

initDbConnection();
initServer();
if (process.env.NODE_ENV != "development") initMail();

function initMail() {

  // create reusable transporter object using the default SMTP transport
  transporter = nodemailer.createTransport({
    host: config.emailSMTP.smtp,
    port: config.emailSMTP.port,
    secure: false, // true for 465, false for other ports
    auth: {
      user: config.emailSMTP.username, // generated ethereal user
      pass: config.emailSMTP.password // generated ethereal password
    },
    pool: true,
    maxConnections: 20,
    rateDelta: 30000,
    rateLimit: 20
  });

  logger.log('SMTP server ready');

}

function initServer() {

  /**
  * Configure APP parameters and application wide locals used in pug
  */

  var port = process.env.PORT || config.general.port;

  app.set('port', port);
  app.locals.moment = require('moment-timezone');
  app.locals.idCompany = config.cenAPI.idCompanyDefault;
  app.locals.lastUPDTcen = "2012"

  /**
   * Create HTTP server.
   */

  if (httpServer !== null) {
    httpServer.close();
    httpServer = null;
  }
  httpServer = http.createServer(app);
  httpServer.listen(port);
  httpServer.on('error', onError);
  httpServer.on('listening', function () {
    logger.log(`HTTP server listening on port ` + port);
  });

}


function initDbConnection() {

  models.sequelize.sync().then(function () {
    return initAPI();
  });

}


function initAPI() {

  cen = new CEN_API(config);
  facturacion_cl = new FacturacionCL_API(config);
  higeco_driver = new HigecoPortalDriver(config);

  initJobs();
}

// job starts
function initJobs() {

  cen.getPlants(function (err) {
    if (err) logger.log(err);
    logger.log("GET PLANTS INFO COMPLETED");
    

    if (process.env.NODE_ENV != "development") {
      logger.log('JOB DATA TYPES STARTED');
      cen.refreshDataTypes(function (err) {
        if (err) logger.log(err);
        logger.log('JOB DATA TYPES FINISHED');
      });

      logger.log('JOB READ DATA STARTED');
      cen.refreshData(function (err) {
        if (err) logger.log(err);
        logger.log('JOB READ DATA FINISHED');
      });
    }
    
    logger.log('JOB MEAS STARTED');
    higeco_driver.refreshData(function (err) {
      if (err) logger.log(err);
      logger.log('JOB MEAS FINISHED');
    });
  });


  if (process.env.NODE_ENV != "development") {

    const jobMEAS = new CronJob(higeco_driver.config.higecoAPI.refreshPeriod, function () {
      logger.log("JOB MEAS START")
      higeco_driver.refreshData(function (err) {
        if (err) logger.log(err);
        logger.log('JOB MEAS FINISHED');
      });

    });
    jobMEAS.start();

    const jobDT = new CronJob(cen.config.cenAPI.refreshPeriod, function () {
      logger.log('JOB DATA TYPES STARTED');
      cen.refreshDataTypes(function (err) {
        if (err) logger.log(err);
        logger.log('JOB DATA TYPES FINISHED');
      });
    });
    jobDT.start();


    const jobRD = new CronJob(cen.config.cenAPI.refreshPeriod, function () {
      logger.log('JOB READ DATA STARTED');
      cen.refreshData(function (err) {
        if (err) logger.log(err);
        logger.log('JOB READ DATA FINISHED');
      });
    });
    jobRD.start();



    const jobEmail = new CronJob(this.config.emailSMTP.refreshPcheckP, function () {
      logger.log('JOB EMAIL STARTED');
      cen.checkPayments(function (err) {
        if (err) logger.log(err);
        logger.log('JOB EMAIL FINISHED');
      });
    });
    jobEmail.start();
  }

}


/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + config.general.port
    : 'Port ' + config.general.port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.log(bind + ' requires elevated privileges', "err");
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.log(bind + ' is already in use', "err");
      process.exit(1);
      break;
    default:
      throw error;
  }
}


function checkConfig() {

  if (!config || !config.general.port) {
    console.log("FATAL ERROR, Config file is not valid, error loading server configuration!");
    return false;
  } else {

    // LOG defaults
    if (!config.debug.LOG) {
      console.log("###  MISSING LOG config, using default one.", "war");
      config.debug.LOG = { level: "dbg", file: false, stdout: true, color: true, maxLogLength: 1000 };
    }

    return true;
  }
}
