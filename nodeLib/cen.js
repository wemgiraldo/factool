var request = require('request');

const Sequelize = require('sequelize');
const op = Sequelize.Op;

const operatorsAliases = {
    $eq: op.eq,
    $or: op.or,
    $and: op.and,
    $between: op.between,
    $gte: op.gte,
    $lte: op.lte,
    $in: op.in
    /*
    [Op.and]: {a: 5}           // AND (a = 5)
    [Op.or]: [{a: 5}, {a: 6}]  // (a = 5 OR a = 6)
    [Op.gt]: 6,                // > 6
    [Op.gte]: 6,               // >= 6
    [Op.lt]: 10,               // < 10
    [Op.lte]: 10,              // <= 10
    [Op.ne]: 20,               // != 20
    [Op.eq]: 3,                // = 3
    */
}

class CEN {

    constructor(config) {

        this.env = process.env.NODE_ENV || 'production';
        this.config = config;
        this.username = this.config.cenAPI.username;
        this.password = this.config.cenAPI.password;
        this.authtoken = "";
        this.idCompany = 0;
        this.plants = {};
        this.created_after;


        if (process.env.NODE_ENV == "development") {
            this.endpoint = this.config.cenAPI.endpoint; //this.config.cenAPI.endpointTest;
        } else {
            this.endpoint = this.config.cenAPI.endpoint;
        }

        // GET Token
        this.getToken(this.username, this.password, function (err, token) {
            cen.authtoken = token;
        });      
            
    }

    getPlants(callback) {
        logger.log("START GET PLANTS");

        models.plants.findAll({ where: { enable: 1 } }).then(plants => {
            cen.plants = plants
            return callback();
        });
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async getFileFromUrl(url, path, callback) {
        var fs = require('fs');

        if (!url) return callback("no url", false);
        logger.log(url);

        await this.sleep(5000);

        const req = request
            .get(url)
            .on('response', function (res) {
                if (res.statusCode === 200) {
                    req.pipe(fs.createWriteStream(path))
                        .on('error', function (err) {
                            logger.log(err);
                            callback(err, false);
                        })
                        .on('finish', function () {
                            callback(null, true);
                        });
                }
            })


    }

    putAuxiliaryFiles(data, callback) {

        var fs = require('fs');
        var path = require('path');
        var path = path.join(global.appRoot, '/public/invoice/pdf/C' + data.company + 'F' + data.folio + 'T' + data.type + '.pdf');

        //logger.log(data);
        this.getFileFromUrl(data.urlOriginal, path, function (err, result) {

            if (err) {
                return callback(err, null, null);
            }

            if (result) {
                request({
                    method: 'PUT',
                    /* preambleCRLF: true,
                     postambleCRLF: true,*/
                    headers: {
                        'Authorization': 'Token ' + cen.authtoken,
                        'Content-Disposition': 'attachment; filename=F' + data.folio + 'T' + data.type + '.pdf'
                    },
                    uri: cen.endpoint + "/api/v1/resources/auxiliary-files/",
                    multipart: [
                        {
                            'content-type': 'application/x-www-form-urlencoded; charset=utf8, multipart/form-data',
                            body: fs.createReadStream(path)
                        }
                    ]
                }, function (error, response, body) {
                    if (error) {
                        console.log('error:', error); // Print the error if one occurred
                        return callback(error);
                    }
                    try {
                        var result = JSON.parse(body);
                        return callback(null, result.invoice_file_id, result.file_url);
                    } catch (e) {
                        console.log('error:', e); // Print the error if one occurred
                        return callback(e);
                    }
                });
            }
        });
    }

    postCreateDte(data, callback) {

        var create_at = new Date(data.created_at);

        if (data.reported_by_creditor) {
            var dataString = {
                instruction: data.instruction,
                type_sii_code: data.type,
                folio: data.folio,
                gross_amount: data.gross_amount,
                net_amount: data.net_amount,
                reported_by_creditor: data.reported_by_creditor,
                emission_dt: moment(create_at).format("Y-M-D"),
                emission_file: data.invoice_file_id_cen,
                emission_erp_a: "",
                emission_erp_b: "",
                reception_dt: moment(create_at).format("Y-M-D"),
                reception_erp: "",
                acceptance_erp: "",
                acceptance_status: ""
            }
        } else {
            var dataString = {
                instruction: data.instruction,
                type_sii_code: data.type,
                folio: data.folio,
                gross_amount: data.gross_amount,
                net_amount: data.net_amount,
                reported_by_creditor: data.reported_by_creditor,
                emission_erp_a: "",
                emission_erp_b: "",
                reception_erp: "",
                acceptance_dt: moment(data.acceptance_dt).format("Y-M-D"),
                acceptance_erp: "",
                acceptance_status: data.acceptance_status
            }
        }

        request.post({
            headers: {
                'Authorization': 'Token ' + this.authtoken
            },
            url: this.endpoint + '/api/v1/operations/dtes/create/',
            formData: { data: JSON.stringify(dataString) }
        }, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                if (result.errors.length) return callback(result.errors, false)
                return callback(null, true);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });


    }

    postDeleteDte(dteid, callback) {

        request.post({
            headers: {
                'Authorization': 'Token ' + this.authtoken
            },
            url: this.endpoint + '/api/v1/operations/dtes/' + dteid + '/delete/'
        }, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                if (result.errors.length) return callback(result.errors, false)
                return callback(null, true);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });

    }

    postAcceptedDte(data, callback) {

        var accepted_at = new Date();

        var dataString = {
            folio: data.folio,
            gross_amount: data.gross_amount,
            net_amount: data.net_amount,
            reported_by_creditor: false,
            acceptance_dt: moment(accepted_at).format("Y-M-D"),
            acceptance_erp: "",
            acceptance_status: 1
        }

        request.post({
            headers: {
                'Authorization': 'Token ' + this.authtoken
            },
            url: this.endpoint + '/api/v1/operations/dtes/' + data.id_cen + '/edit/',
            formData: { data: JSON.stringify(dataString) }
        }, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                if (result.errors.length) return callback(result.errors, false)
                return callback(null, true);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });

    }

    postRejectedDte(data, callback) {

        var rejected_at = new Date();

        var dataString = {
            folio: data.folio,
            gross_amount: data.gross_amount,
            net_amount: data.net_amount,
            reported_by_creditor: false,
            acceptance_dt: moment(rejected_at).format("Y-M-D"),
            acceptance_erp: "",
            acceptance_status: 2
        }

        request.post({
            headers: {
                'Authorization': 'Token ' + this.authtoken
            },
            url: this.endpoint + '/api/v1/operations/dtes/' + data.id_cen + '/edit/',
            formData: { data: JSON.stringify(dataString) }
        }, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                if (result.errors.length) return callback(result.errors, false)
                return callback(null, true);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });

    }

    postCreatePayment(data, callback) {

        var dataString = {
            debtor: data.debtor,
            creditor: data.creditor,
            amount: data.amount,
            payment_dt: data.payment_dt,
            transaction_type: data.transaction_type,
            actual_collector: data.actual_collector,
            instruction_amount_tuples: data.instruction_amount_tuples
        }

        request.post({
            headers: {
                'Authorization': 'Token ' + this.authtoken
            },
            url: this.endpoint + '/api/v1/operations/payments/create/',
            formData: { data: JSON.stringify(dataString) }
        }, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                if (result.errors.length) return callback(result.errors, false)
                return callback(null, true);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });

    }

    getToken(username, password, callback) {

        request.post({
            headers: { 'content-type': 'application/json' },
            url: this.endpoint + '/api/token-auth/',
            json: {
                "username": username,
                "password": password
            }
        }, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            return callback(null, body.token);
        });
    }

    getBillingStatusType(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/billing-status-type/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getCompany(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/participants/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getBanks(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/banks/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getBillingType(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/billing-types/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getBillingWindows(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/billing-windows/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getDteTypes(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/dte-types/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getPaymentDueType(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/payment-due-type/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getPaymentStatusType(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/payment-status-type/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getDteAcceptanceStatus(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/dte-acceptance-status/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getTransactionTypes(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/transaction-types/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getInstructions(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/instructions/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                if (result.next) {
                    request(result.next, function (error, response, body) {

                    })
                }
                callback(result);
            } catch (e) {
                console.log(body);
                console.log(response);
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getPaymentMatrices(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/payment-matrices/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getPaymentExecutions(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/payment-execution-payment-instructions/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    getDte(filters, callback) {

        if (typeof (filters) === "function" && typeof (callback) === "undefined") {
            callback = filters;
            filters = {};
        }

        var str_filter = "";
        for (var i = 0; i < Object.keys(filters).length; i++) {
            var filter = Object.keys(filters)[i];
            if (i === 0) str_filter = "?";
            str_filter = str_filter + filter + "=" + filters[filter];
            if (i < Object.keys(filters).length - 1) str_filter = str_filter + "&";
        }

        request(this.endpoint + '/api/v1/resources/dtes/' + str_filter, function (error, response, body) {
            if (error) {
                console.log('error:', error); // Print the error if one occurred
                return callback(error);
            }
            try {
                var result = JSON.parse(body);
                callback(result);
            } catch (e) {
                console.log('error:', e); // Print the error if one occurred
                return callback(e);
            }
        });
    }

    // REFRESH DATA TYPES

    refreshDataTypes(callback) {

        var me = this;

        logger.log("START GET DATA TYPES");

        var oggi = moment().add(-16, 'months').startOf('month');
        this.created_after = oggi.toISOString();

        async.parallel([
            function (callback) {
                // GET BillingStatusType DATA
                me.getBillingStatusType({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            name: resp.results[key].name,
                            natural_key: resp.results[key].natural_key
                        }
                        updateOrCreate(models.billing_status_type, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET PaymentStatusType DATA
                me.getPaymentStatusType({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            name: resp.results[key].name,
                            natural_key: resp.results[key].natural_key
                        }
                        updateOrCreate(models.payment_status_type, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET Banks DATA
                me.getBanks({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            code: resp.results[key].code,
                            name: resp.results[key].name,
                            sbif: resp.results[key].sbif,
                            type: resp.results[key].type
                        }
                        updateOrCreate(models.banks, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET BillingType DATA
                me.getBillingType({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            natural_key: resp.results[key].natural_key,
                            title: resp.results[key].title,
                            system_prefix: resp.results[key].system_prefix,
                            description_prefix: resp.results[key].description_prefix,
                            payment_window: resp.results[key].payment_window,
                            department: resp.results[key].department,
                            enabled: resp.results[key].enabled
                        }
                        updateOrCreate(models.billing_type, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET DteAcceptanceStatus DATA
                me.getDteAcceptanceStatus({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            code: resp.results[key].code,
                            name: resp.results[key].name
                        }
                        updateOrCreate(models.dte_acceptance_status, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET DteTypes DATA
                me.getDteTypes({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            code: resp.results[key].code,
                            name: resp.results[key].name,
                            sii_code: resp.results[key].sii_code,
                            factor: resp.results[key].factor
                        }
                        updateOrCreate(models.dte_type, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET PaymentDueType DATA
                me.getPaymentDueType({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            name: resp.results[key].name,
                            natural_key: resp.results[key].natural_key
                        }
                        updateOrCreate(models.payment_due_type, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            },
            function (callback) {
                // GET TransactionTypes DATA
                me.getTransactionTypes({ limit: 5000, offset: 0, created_after: me.created_after }, function (resp) {
                    // IF NO RESULTS -> EXIT
                    if (!resp.results) return
                    if (resp.results.length === 0) return callback("No results for the API response.", false);
                    // UPDATE OR CREATE RECORDS
                    async.forEachOf(resp.results, function (value, key, callback) {
                        var data = {
                            id: resp.results[key].id,
                            code: resp.results[key].code,
                            name: resp.results[key].name
                        }
                        updateOrCreate(models.transaction_type, { id: data.id }, data, function (err) {
                            if (err) return callback(err);
                            return callback();
                        });

                    }, function (err) {
                        if (err) return callback(err)
                        return callback();
                    });
                });
            }
        ],
            // optional callback
            function (err, results) {
                /*if (err) {
                    logger.log("GET DATA TYPES - NOT OK: " + err);
                } else {
                    logger.log("GET DATA TYPES - OK");
                }*/
                return callback(err);
            });

    }



    // CHECK PAYMENTS AND SEND EMAILS

    checkPayments(callback) {

        var me = this;

        logger.log("START CHECK PAYMENTS");

        var emission = moment().subtract(2, 'months').endOf('month').format('YYYY-MM-DD');

        models.sequelize.query(
            "select i.creditor as who, c.name as company_name, c.p_c_email as pay_email, c.b_c_email as dte_email, c.p_c_first_name as first_name, c.p_c_last_name as last_name, GROUP_CONCAT(d.folio) as folios, GROUP_CONCAT(pm.natural_key) as natural_key, GROUP_CONCAT(d.emission_dt) as emission_dt, GROUP_CONCAT(d.gross_amount) as gross_amount, GROUP_CONCAT(d.net_amount) as net_amount, GROUP_CONCAT(i.id_cen) as instrs " +
            "from instructions i " +
            "join company c on i.debtor = c.id " +
            "join dte d on i.id_cen = d.instruction " +
            "join payment_matrices pm on i.payment_matrix = pm.id_cen " +
            "where i.creditor in (373,394,339,381) AND i.status_paid = 1 and (i.status_paid_2 = 1 OR i.status_paid_2 is null) and d.reported_by_creditor = 1 AND ( i.email_sent IS NULL OR i.email_sent = 0 ) AND d.emission_dt <= '" + emission + "' " +
            "group by i.creditor, c.name, c.dte_reception_email, c.p_c_email, c.b_c_email " +
            "limit 5"
            , { type: models.sequelize.QueryTypes.SELECT })
            .then(instructions => {

                //console.log(instructions);

                var html = "";

                instructions.forEach(instr => {
                    html =
                        "<p>Estimado/a " + instr.first_name + " " + instr.last_name + "</p>" +
                        "<p>" + instr.company_name + "</p>" +
                        "<br>" +
                        "<p>Junto con saludarle, le escribimos ya que en los registros de " +
                        cen.getPlantName(instr.who) +
                        " figuran una o más facturas morosas a la fecha, las cuales detallamos en el siguiente cuadro: <p>" +
                        "<table border='1'>" +
                        "<tr>" + // header 
                        "<th>Numero de Factura</th>" +
                        "<th>Sociedad</th>" +
                        "<th>Glosa</th>" +
                        "<th>Fecha de Emisión</th>" +
                        "<th>Monto Neto</th>" +
                        "<th>Monto Total</th>" +
                        "</tr>"

                    var folio = instr.folios.split(",");
                    var natural_key = instr.natural_key.split(",");
                    var emission_dt = instr.emission_dt.split(",");
                    var gross_amount = instr.gross_amount.split(",");
                    var net_amount = instr.net_amount.split(",");
                    var prev;

                    for (var i = 0; i < folio.length; i++) {
                        if (!prev) {
                            prev = folio[i];
                        } else {
                            if (prev == folio[i]) continue;
                        }
                        html = html + "<tr>" + // rows data 
                            "<td>" + folio[i] + "</td>" +
                            "<td>" + instr.company_name + "</td>" +
                            "<td>" + natural_key[i] + "</td>" +
                            "<td>" + emission_dt[i] + "</td>" +
                            "<td>" + net_amount[i] + "</td>" +
                            "<td>" + gross_amount[i] + "</td>" +
                            "</tr>"

                    }

                    html = html + "</table>" +
                        "<p>Lo invitamos a que regularice su situación lo antes posible. Puede contactarse con Wladimir Navia Donoso o enviar un correo a centropagosfachile@gmail.com para saber más detalles.</p>" +
                        "<p>Si al recibir esta comunicación, su situación ya ha sido regularizada, solicitamos nos pueda hacer llegar al correo centropagosfachile@gmail.com los comprobantes de pago, para normalizar en nuestro sistema.</p>" +
                        "<p>En caso de no recibir el pago dentro de 30 días desde la emisión de la presente comunicación, se iniciará un procedimiento de cobranza judicial más los gastos judiciales generados por las acciones establecidas.</p>" +
                        "<p>Saluda atentamente a Usted,</p>" +
                        "<br>" +
                        "<br>" +
                        "<p>Lorena Barria</p>" +
                        "<p>Representante Legal " + cen.getPlantName(instr.who) + "</p>" +
                        "<p>RUT: " + cen.getRUT(instr.who) + "</p>"

                    cen.sendEmail(instr, "Regularizacion de Pago", html, function (err) {
                        if (!err) { // done
                            var filter = {
                                id_cen: {
                                    $in: instr.instrs.split(",")
                                }
                            }
                            models.instructions.findAll({ where: filter }).then(instances => {
                                instances.forEach(function (instance) {
                                    instance.update({ email_sent: 1, email_sent_ts: moment() });
                                });
                            });
                        } else { // not done
                            var filter = {
                                id_cen: {
                                    $in: instr.instrs.split(",")
                                }
                            }
                            models.instructions.findAll({ where: filter }).then(instances => {
                                instances.forEach(function (instance) {
                                    instance.update({ email_sent: 0 });
                                });
                            });
                        }
                    });
                });
            })

        return callback();

       /* var jobEmail = schedule.scheduleJob(this.config.emailSMTP.refreshPcheckP, function() {
            console.log('JOB EMAIL STARTED');
            cen.checkPayments(function () {
                console.log('JOB EMAIL FINISHED');
                return callback();
            });
          });*/


        /*setTimeout(function () {
            cen.checkPayments(function () {
                return callback();
            });
        }, (this.config.emailSMTP.refreshPcheckP));*/


    }


    getPlantName(id, callback) {
        var name;
        cen.plants.forEach(plant => {
            if (plant.company_cen_id === id) {
                name = plant.company_cen_name;
            }
        });
        return name;
    }

    getRUT(id, callback) {
        var rut;
        cen.plants.forEach(plant => {
            if (plant.company_cen_id === id) {
                rut = plant.rut_fact;
            }
        });
        return rut;
    }

    async sendEmail(instr, subject, html, callback) {

        var to;

        if (instr.dte_email) {
            to = instr.dte_email
        }

        if (instr.pay_email) {
            to = to + "," + instr.pay_email
        }

        // send mail with defined transport object
        await transporter.sendMail({
            from: '<centropagosfachile@gmail.com>', // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            //text: "Hello world?", // plain text body
            html: html // html body
        }).then(async function (info) {
            console.log("Message sent: %s", info.messageId);
            return callback();
        }).catch(function (err) {
            console.log(err);
            return callback(err);
        });

    }

    refreshData(options, callback) {

        if (typeof (options) === "function" && typeof (callback) === "undefined") {
            callback = options;
            options = undefined;
        }

        var me = this;

        var oggi = moment().add(-5, 'months').startOf('month');
        me.created_after = oggi.toISOString();
        var annofa = moment().add(-12, 'months').startOf('month');
        me.created_after_long = annofa.toISOString();

        app.locals.lastUPDTcen = moment().tz("America/Santiago").format('YYYY-MM-DD HH:mm');

        if (options) {
            if (options.id) {

                logger.log("START GET DATA " + options.filter + " FOR IDCOMPANY: " + options.id);

                switch (options.filter) {
                    case "Company":
                        updateCompany(me, { limit: 5000, offset: 0 }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "BillingWindows":
                        updateBillingWindows(me, { limit: 5000, offset: 0, created_after: me.created_after_long }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "InstructionsC":
                        updateInstructions(me, { creditor: options.id, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "InstructionsD":
                        updateInstructions(me, { debtor: options.id, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "PaymentMatrices":
                        updatePaymentMatrices(me, { limit: 5000, offset: 0, created_after: me.created_after_long }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "PaymentExecutions":
                        updatePaymentExecutions(me, { creditor: idCompany, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "DteC":
                        updateDte(me, { creditor: options.id, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                    case "DteD":
                        updateDte(me, { debtor: options.id, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                            if (err) return callback(err);
                            logger.log("GET DATA " + options.filter + " - OK");
                            return callback();
                        });
                        break;
                }

            } else {

                async.forEachOf(me.plants, function (value, key, callback) {
                    logger.log("START GET DATA " + options + " FOR IDCOMPANY: " + idCompany);

                    switch (options) {
                        case "Company":
                            updateCompany(me, { limit: 5000, offset: 0 }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "BillingWindows":
                            updateBillingWindows(me, { limit: 5000, offset: 0, created_after: me.created_after_long }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "InstructionsC":
                            updateInstructions(me, { creditor: idCompany, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "InstructionsD":
                            updateInstructions(me, { debtor: idCompany, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "PaymentMatrices":
                            updatePaymentMatrices(me, { limit: 10000, offset: 0, created_after: me.created_after_long }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "PaymentExecutions":
                            updatePaymentExecutions(me, { creditor: idCompany, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "DteC":
                            updateDte(me, { creditor: idCompany, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                        case "DteD":
                            updateDte(me, { debtor: idCompany, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                                if (err) return callback(err);
                                logger.log("GET DATA " + options + " - OK");
                                return callback();
                            });
                            break;
                    }
                }, function (err) {
                    if (err) return callback(err);
                    return callback();
                });
            }
        } else {
            //async.forEachOf(me.plants, function (value, key, callback) {

            var idCompany = me.plants.map(p => p.company_cen_id).join(",");
            // var prova = map( p => p.company_cen_id ).join(",");

            logger.log("START GET DATA FOR IDCOMPANY: " + idCompany);

            async.parallel([
                function (callback) {
                    updateCompany(me, { limit: 5000, offset: 0 }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updateDte(me, { creditor: idCompany, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updateDte(me, { debtor: idCompany, limit: 10000, offset: 0, created_after: me.created_after }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updatePaymentMatrices(me, { limit: 5000, offset: 0 }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updatePaymentExecutions(me, { creditor: idCompany, limit: 5000, offset: 0 }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updateBillingWindows(me, { limit: 5000, offset: 0 }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updateInstructions(me, { creditor: idCompany, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                },
                function (callback) {
                    updateInstructions(me, { debtor: idCompany, limit: 5000, offset: 0, created_after: me.created_after }, function (err, result) {
                        if (err) return callback(err);
                        return callback();
                    });
                }
            ],
                // optional callback
                function (err, results) {
                    if (err) return callback(err);
                    logger.log("GET DATA " + idCompany + " - OK");

                    /*setTimeout(function () {

                        cen.refreshData(function (err) {
                            if (err) logger.log(err);
                        });

                    }, (cen.config.cenAPI.refreshPeriod));*/

                
                    return callback();
                });


            /*}, function (err) {

                setTimeout(function () {

                    cen.refreshData(function (err) {
                        if (err) logger.log(err);
                    });

                }, (cen.config.cenAPI.refreshPeriod));

                return callback();
            });*/
        }
    }
}


function updateOrCreate(model, where, data, callback) {

    model.upsert(data)
        .then(function (created) {
            return callback(null, created);
        }).catch((error) => {
            console.log(error);
            return callback(error, false);
        });

    /*
model.findOrCreate({
    where: where,
    defaults: data
})
    .spread((record, created) => {
        if (created) {
            return callback();
        } else {
            record.update(data)
                .then(function () {
                    return callback();
                })
        }
    })
    .catch(function (error) {
        return callback(error);
    });*/

    /*
    return model.findOrCreate({ where: where })
        .spread((record, created) => {
            // IF ALREADY EXISTS, I UPDATE THE DATA
            // ELSE I CREATE THE RECORD AND ADD THE NEW DATA

            record.update(data)
                .then(() => {
                    return callback(null, true);
                })
                .catch(() => {
                    console.log(error);
                    return callback(error, false);
                });

        })
        .catch(function (error) {
            console.log(error);
            return callback(error, false);
        });*/
}

function updateCompany(cen, filter, callback) {
    cen.getCompany(filter, function (resp) {
        // IF NO RESULTS -> EXIT
        if (!resp.results) return callback("Connection problem - API no work.", false);
        if (resp.results.length === 0) return callback();
        // UPDATE OR CREATE RECORDS
        async.forEachOf(resp.results, function (value, key, callback) {
            var data = {
                id: resp.results[key].id,
                name: resp.results[key].name,
                rut: resp.results[key].rut,
                verification_code: resp.results[key].verification_code,
                business_name: resp.results[key].business_name,
                commercial_business: resp.results[key].commercial_business,
                dte_reception_email: resp.results[key].dte_reception_email,
                bank_account: resp.results[key].bank_account,
                bank: resp.results[key].bank,
                commercial_address: resp.results[key].commercial_address,
                postal_address: resp.results[key].postal_address,
                manager: resp.results[key].manager,
                p_c_first_name: resp.results[key].payments_contact ? resp.results[key].payments_contact.first_name : "",
                p_c_last_name: resp.results[key].payments_contact ? resp.results[key].payments_contact.last_name : "",
                p_c_address: resp.results[key].payments_contact ? resp.results[key].payments_contact.address : "",
                p_c_phones: resp.results[key].payments_contact ? resp.results[key].payments_contact.phones.toString() : "",
                p_c_email: resp.results[key].payments_contact ? resp.results[key].payments_contact.email : "",
                b_c_first_name: resp.results[key].bills_contact ? resp.results[key].bills_contact.first_name : "",
                b_c_last_name: resp.results[key].bills_contact ? resp.results[key].bills_contact.last_name : "",
                b_c_address: resp.results[key].bills_contact ? resp.results[key].bills_contact.address : "",
                b_c_phones: resp.results[key].bills_contact ? resp.results[key].bills_contact.phones.toString() : "",
                b_c_email: resp.results[key].bills_contact ? resp.results[key].bills_contact.email : "",
                created_ts: resp.results[key].created_ts,
                updated_ts: resp.results[key].updated_ts
            }
            updateOrCreate(models.company, { id: data.id }, data, function (err) {
                if (err) return callback(err);
                return callback();
            });

        }, function (err) {
            if (err) return callback(err)
            return callback();
        });
    });

}

function updateInstructions(cen, filter, callback) {
    // GET Instructions Debtor DATA
    cen.getInstructions(filter, function (resp) {
        // IF NO RESULTS -> EXIT
        if (!resp.results) return callback("Connection problem - API no work.", false);
        if (resp.results.length === 0) return callback();
        // UPDATE OR CREATE RECORDS
        async.forEachOf(resp.results, function (value, key, callback) {
            var data = {
                id: resp.results[key].id,
                id_cen: resp.results[key].id,
                payment_matrix: resp.results[key].payment_matrix,
                creditor: resp.results[key].creditor,
                debtor: resp.results[key].debtor,
                amount: resp.results[key].amount,
                amount_gross: Math.round(resp.results[key].amount * 1.19),
                closed: resp.results[key].closed === false ? "false" : "true",
                status: resp.results[key].status,
                status_billed: resp.results[key].status_billed,
                status_paid: resp.results[key].status_paid,
                resolution: resp.results[key].resolution,
                max_payment_date: resp.results[key].max_payment_date,
                informed_paid_amount: resp.results[key].informed_paid_amount,
                is_paid: resp.results[key].is_paid,
                aux_data_payment_matrix_natural_key: resp.results[key].aux_data_payment_matrix_natural_key,
                aux_data_payment_matrix_concept: resp.results[key].aux_data_payment_matrix_concept,
                created_ts: resp.results[key].created_ts,
                updated_ts: resp.results[key].updated_ts
            }
            updateOrCreate(models.instructions, { id_cen: data.id_cen }, data, function (err) {
                if (err) return callback(err);
                return callback();
            });

        }, function (err) {
            if (err) return callback(err)
            return callback();
        });
    });

}

function updatePaymentMatrices(cen, filter, callback) {
    // GET PaymentMatrices DATA
    cen.getPaymentMatrices(filter, function (resp) {
        // IF NO RESULTS -> EXIT
        if (!resp.results) return callback("Connection problem - API no work.", false);
        if (resp.results.length === 0) return callback();
        // UPDATE OR CREATE RECORDS
        async.forEachOf(resp.results, function (value, key, callback) {
            var data = {
                id: resp.results[key].id,
                id_cen: resp.results[key].id,
                auxiliary_data: resp.results[key].auxiliary_data.toString(),
                created_ts: resp.results[key].created_ts,
                updated_ts: resp.results[key].updated_ts,
                payment_type: resp.results[key].payment_type,
                version: resp.results[key].version,
                payment_file: resp.results[key].payment_file,
                letter_code: resp.results[key].letter_code,
                letter_year: resp.results[key].letter_year,
                letter_file: resp.results[key].letter_file,
                matrix_file: resp.results[key].matrix_file,
                publish_date: resp.results[key].publish_date,
                payment_days: resp.results[key].payment_days,
                payment_date: resp.results[key].payment_date,
                billing_date: resp.results[key].billing_date,
                payment_window: resp.results[key].payment_window,
                natural_key: resp.results[key].natural_key,
                reference_code: resp.results[key].reference_code,
                billing_window: resp.results[key].billing_window,
                payment_due_type: resp.results[key].payment_due_type
            }
            updateOrCreate(models.payment_matrices, { id_cen: data.id_cen }, data, function (err) {
                if (err) return callback(err);
                return callback();
            });

        }, function (err) {
            if (err) return callback(err)
            return callback();
        });
    });

}

function updatePaymentExecutions(cen, filter, callback) {
    // GET updatePaymentExecutions DATA
    cen.getPaymentExecutions(filter, function (resp) {
        // IF NO RESULTS -> EXIT
        if (!resp.results) return callback("Connection problem - API no work.", false);
        if (resp.results.length === 0) return callback();
        // UPDATE OR CREATE RECORDS
        async.forEachOf(resp.results, function (value, key, callback) {
            var data = {
                id: resp.results[key].id,
                id_cen: resp.results[key].id,
                instruction: resp.results[key].instruction,
                execution: resp.results[key].execution,
                amount: resp.results[key].amount,
                is_net_amount: resp.results[key].is_net_amount,
                dtes: resp.results[key].dtes.toString(),
                created_ts: resp.results[key].created_ts,
                updated_ts: resp.results[key].updated_ts
            }
            updateOrCreate(models.payment_executions, { id_cen: data.id_cen }, data, function (err) {
                if (err) return callback(err);
                return callback();
            });

        }, function (err) {
            if (err) return callback(err)
            return callback();
        });
    });

}

function updateDte(cen, filter, callback) {
    // GET DTEs DATA
    cen.getDte(filter, function (resp) {
        // IF NO RESULTS -> EXIT
        if (!resp.results) return callback("Connection problem - API no work.", false);
        if (resp.results.length === 0) return callback();
        // UPDATE OR CREATE RECORDS
        async.forEachOf(resp.results, function (value, key, callback) {
            var data = {
                id: resp.results[key].id,
                id_cen: resp.results[key].id,
                instruction: resp.results[key].instruction,
                type: resp.results[key].type,
                folio: resp.results[key].folio,
                gross_amount: resp.results[key].gross_amount,
                net_amount: resp.results[key].net_amount,
                reported_by_creditor: resp.results[key].reported_by_creditor,
                emission_dt: resp.results[key].emission_dt,
                emission_file: resp.results[key].emission_file,
                emission_erp_a: resp.results[key].emission_erp_a,
                emission_erp_b: resp.results[key].emission_erp_b,
                reception_dt: resp.results[key].reception_dt,
                reception_erp: resp.results[key].reception_erp,
                acceptance_dt: resp.results[key].acceptance_dt,
                acceptance_erp: resp.results[key].acceptance_erp,
                acceptance_status: resp.results[key].acceptance_status,
                created_ts: resp.results[key].created_ts,
                updated_ts: resp.results[key].updated_ts
            }
            updateOrCreate(models.dte, { id_cen: data.id_cen }, data, function (err) {
                if (err) return callback(err);
                return callback();
            });

        }, function (err) {
            if (err) return callback(err)
            return callback();
        });
    });
}

function updateBillingWindows(cen, filter, callback) {
    // GET BillingWindows DATA
    cen.getBillingWindows(filter, function (resp) {
        // IF NO RESULTS -> EXIT
        if (!resp.results) return callback("Connection problem - API no work.", false);
        if (resp.results.length === 0) return callback();
        // UPDATE OR CREATE RECORDS
        async.forEachOf(resp.results, function (value, key, callback) {
            var data = {
                id: resp.results[key].id,
                natural_key: resp.results[key].natural_key,
                billing_type: resp.results[key].billing_type,
                periods: resp.results[key].periods.toString(),
                created_ts: resp.results[key].created_ts,
                updated_ts: resp.results[key].updated_ts
            }
            updateOrCreate(models.billing_windows, { id: data.id }, data, function (err) {
                if (err) return callback(err);
                return callback();
            });

        }, function (err) {
            if (err) return callback(err)
            return callback();
        });
    });
}

module.exports = CEN;
