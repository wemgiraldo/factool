var soap = require('soap');
var convertXML = require('xml-js');
var async = require('async');
var request = require('request');

class FacturacionCL {

    constructor(config) {

        this.env = process.env.NODE_ENV || 'production';
        this.endpoint = "http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl=0";
        this.endpoint_local = path.join(global.appRoot, '/public/wsdl/wsplano.wsdl');
        this.config = config;

        this.gmapApiKey = this.config.vendor.gmapApiKey;

        /* this.verificationAddress("Santa Rosa 76 Santiago", function (err, result) {
             if (err) console.log(err);
             console.log(result);
         });*/
    }

    verificationAddress(address, callback) {

        var me = this;
        var addr = address.replace(/ /g, "+");
        // Vitacura 2969 Of. 902 Las Condes Santiago
        // Vitacura+2969+Of.+902+Las+Condes+Santiago
        console.log('address:', addr);
        async.waterfall([
            function (callback) {
                request.post({
                    headers: { 'content-type': 'application/json' },
                    url: 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?&key=' + me.gmapApiKey + '&input=' + encodeURIComponent(addr) + '&inputtype=textquery',
                }, function (error, response, body) {
                    if (error) {
                        logger.log('error:', error); // Print the error if one occurred
                        return callback(error);
                    }
                    var result = JSON.parse(body);
                    console.log('findplace:', result);
                    if (result.status == "OK") {
                        var place_id = result.candidates[0].place_id;
                        return callback(null, place_id);
                    } else {
                        logger.log('error:', result.status + " " + result.error_message); // Print the error if one occurred
                        return callback(result.status + " " + result.error_message);
                    }
                });
            },
            function (place_id, callback) {

                request.post({
                    headers: { 'content-type': 'application/json' },
                    url: 'https://maps.googleapis.com/maps/api/place/details/json?&key=' + me.gmapApiKey + '&placeid=' + place_id,
                }, function (error, response, body) {
                    if (error) {
                        logger.log('error:', error); // Print the error if one occurred
                        return callback(error);
                    }
                    var result = JSON.parse(body);
                    console.log('placedetails:', result);
                    if (result.status == "OK") {
                        var info_place = {
                            /*country: result.result.address_components.filter(function (el) {
                                return (el.types.indexOf("country") > -1)
                            })[0].long_name,
                            city: result.result.address_components.filter(function (el) {
                                return (el.types.indexOf("administrative_area_level_2") > -1)
                            })[0].long_name,
                            locality: result.result.address_components.filter(function (el) {
                                return (el.types.indexOf("administrative_area_level_3") > -1)
                            })[0].long_name,*/

                            country: (result.result.address_components.filter(function (el) { return (el.types.indexOf("country") > -1) })[0]) ? result.result.address_components.filter(function (el) { return (el.types.indexOf("country") > -1) })[0].long_name : "",
                            city: (result.result.address_components.filter(function (el) { return (el.types.indexOf("administrative_area_level_2") > -1) })[0]) ? result.result.address_components.filter(function (el) { return (el.types.indexOf("administrative_area_level_2") > -1) })[0].long_name : "",
                            locality: (result.result.address_components.filter(function (el) { return (el.types.indexOf("administrative_area_level_3") > -1) })[0]) ? result.result.address_components.filter(function (el) { return (el.types.indexOf("administrative_area_level_3") > -1) })[0].long_name : "",
                            route: (result.result.address_components.filter(function (el) { return (el.types.indexOf("route") > -1) })[0]) ? result.result.address_components.filter(function (el) { return (el.types.indexOf("route") > -1) })[0].long_name : "",
                            street_number: (result.result.address_components.filter(function (el) { return (el.types.indexOf("street_number") > -1) })[0]) ? result.result.address_components.filter(function (el) { return (el.types.indexOf("street_number") > -1) })[0].long_name : "",
                            /*additional_info: result.result.address_components.filter(function (el) {
                                return (el.types.indexOf("subpremise") > -1)
                            })[0].long_name*/
                        }
                        return callback(null, info_place);
                    }
                });
            }
        ], function (error, info_place) {
            if (error) return callback(error)
            return callback(null, info_place);
        });

    }

    createNotaCredito61XML(instruction, payment_matrix, d, callback) {

        var debtor = instruction.debtor_info;
        var creditor = instruction.creditor_info;
        var dte = d[0];

        if (!dte.folio) {
            logger.log("ERROR: no se puede hacer la nota de credito porque falta la factura de referencia");
            return callback("ERROR: no se puede hacer la nota de credito porque falta la factura de referencia");
        }

        getLastNota(instruction.creditor, function (err, result) {

            if (err) {
                logger.log("error");
                return callback(err);
            }

            var nota = result; // NOTA NUMBER
            var info_place = {};

            facturacion_cl.verificationAddress(debtor.commercial_address.substring(0, 60), function (err, result) {
                if (err) {
                    console.log(err);
                    info_place = creditor.commercial_address.substring(0, 60);
                } else {
                    info_place = result;
                }

                var fechaEmis;
                if (facturacion_cl.config.notaManual.enabled == "1") {
                    fechaEmis = facturacion_cl.config.notaManual.date;
                } else {
                    fechaEmis = moment();
                }

                var data = {
                    _declaration: {
                        _attributes: { version: "1.0", encoding: "ISO-8859-1" }
                    },
                    /*
                                    Caratula: {
                                        _attributes: { version: "1.0" },
                                        RutEmisor: numberWithThousands(creditor.rut) + "-" + creditor.verification_code,
                                        RutEnvia: "10890592-1",
                                        RutReceptor: numberWithThousands(debtor.rut) + "-" + debtor.verification_code,
                                        FchResol: "2014-08-22",
                                        NroResol: "80",
                                        TmstFirmaEnv: "2019-01-28T10:51:49",
                                        SubTotDTE: {
                                            TpoDTE: 61,
                                            NroDTE: 1
                                        }
                                    },*/
                    DTE: {
                        _attributes: { version: "1.0" },
                        Documento: {
                            _attributes: { ID: "F" + nota + "T61" },
                            Encabezado: {
                                IdDoc: {
                                    TipoDTE: 61,
                                    Folio: nota,
                                    FchEmis: moment(fechaEmis).format("YYYY-MM-DD"),
                                    TpoTranVenta: 1
                                },
                                Emisor: {
                                    RUTEmisor: numberWithThousands(creditor.rut) + "-" + creditor.verification_code,
                                    RznSoc: creditor.business_name.substring(0, 100),
                                    GiroEmis: creditor.commercial_business.substring(0, 40),
                                    CorreoEmisor: "cobranzas@buildingenergy.it",
                                    Acteco: 401019,
                                    DirOrigen: creditor.commercial_address.substring(0, 60),
                                    CmnaOrigen: "",
                                    CiudadOrigen: ""
                                },
                                Receptor: {
                                    RUTRecep: numberWithThousands(debtor.rut) + "-" + debtor.verification_code,
                                    RznSocRecep: debtor.business_name.substring(0, 100),
                                    GiroRecep: debtor.commercial_business.substring(0, 40),
                                    Contacto: "",
                                    DirRecep: (info_place.route) ? info_place.route + " N° " + info_place.street_number : debtor.commercial_address.substring(0, 60),
                                    CmnaRecep: (info_place.locality) ? info_place.locality : "",
                                    CiudadRecep: (info_place.city) ? info_place.city : ""
                                },
                                Totales: {
                                    MntNeto: instruction.amount,
                                    //MntExe: 0,
                                    TasaIVA: 19,
                                    IVA: instruction.amount_gross - instruction.amount,
                                    MntTotal: instruction.amount_gross
                                }
                            },
                            Detalle: {
                                NroLinDet: 1,
                                CdgItem: {
                                    TpoCodigo: "INT1",
                                    VlrCodigo: "4-1-1-01-01"
                                },
                                NmbItem: payment_matrix.natural_key,
                                DscItem: payment_matrix.billingWindow.billingType.title + " (Carta " + payment_matrix.letter_code + ", balance " + payment_matrix.billingWindow.periods + ")",
                                QtyItem: 1,
                                UnmdItem: "UN",
                                PrcItem: instruction.amount,
                                MontoItem: instruction.amount
                            },
                            Referencia: [{
                                NroLinRef: 1,
                                TpoDocRef: "33",
                                FolioRef: dte.folio,
                                FchRef: dte.emission_dt,
                                CodRef: "1",
                                RazonRef: "DOCUMENTO MAL EMITIDO"
                            }]
                        }
                    }
                }


                // CONVERT JS OBJECT TO XML FILE AND ENCODE IT IN 64BIT FOR USE IN SOAP REQ
                var json2xml = convertXML.js2xml(data, { compact: true, spaces: 4 });
                var xmlEncoded64 = Buffer.from(json2xml).toString('base64');

                // SAVE THE XML IN A LOCAL FOLDER 
                var xml = require('fs').writeFileSync(path.join(global.appRoot, '/public/nota/xml/F' + nota + 'T61.xml'), json2xml, 'utf8');

                getLogin(instruction.creditor, function (err, result) {
                    facturacion_cl.loadNota(instruction, dte, xmlEncoded64, "2", result, function (err, result) {
                        if (err) {
                            logger.log(err);
                            return callback(err, result);
                        }
                        callback(null, result);
                    });
                });
            });
        });
    }

    createInvoice33XML(instruction, payment_matrix, callback) {

        var debtor = instruction.debtor_info;
        var creditor = instruction.creditor_info;

        getLastFolio(instruction.creditor, function (err, result) {

            if (err) {
                logger.log("error");
                return callback(err);
            }

            var folio = result; // INVOICE NUMBER
            if (facturacion_cl.env == "development") folio = getRandomArbitrary(40000, 50000);
            var info_place = {};

            facturacion_cl.verificationAddress(debtor.commercial_address.substring(0, 60), function (err, result) {
                if (err) {
                    console.log(err);
                    info_place = debtor.commercial_address.substring(0, 60);
                } else {
                    info_place = result;
                }

                var fechaEmis;
                if (facturacion_cl.config.facturacionManual.enabled == "1") {
                    fechaEmis = facturacion_cl.config.facturacionManual.date;
                } else {
                    fechaEmis = moment();
                }

                if (debtor.code_for_fact) {

                    var data = {
                        _declaration: {
                            _attributes: { version: "1.0", encoding: "ISO-8859-1" }
                        },
                        DTE: {
                            _attributes: { version: "1.0" },
                            Documento: {
                                _attributes: { ID: "F" + folio + "T33" },
                                Encabezado: {
                                    /*
                                    IdDoc: {
                                        TipoDTE: 33,
                                        Folio: folio,
                                        FchEmis: moment("2019-03-30").format("YYYY-MM-DD"), //moment().format("YYYY-MM-DD"),
                                        FmaPago: 2,
                                        TermPagoGlosa: "CREDITO SII",
                                        FchVenc: moment("2019-03-30").add(8, "day").format("YYYY-MM-DD")
                                    },*/
                                    IdDoc: {
                                        TipoDTE: 33,
                                        Folio: folio,
                                        FchEmis: moment(fechaEmis).format("YYYY-MM-DD"),
                                        FmaPago: 2,
                                        TermPagoGlosa: "CREDITO SII",
                                        FchVenc: moment(fechaEmis).add(8, "day").format("YYYY-MM-DD")
                                    },
                                    Emisor: {
                                        RUTEmisor: numberWithThousands(creditor.rut) + "-" + creditor.verification_code,
                                        RznSoc: creditor.business_name.substring(0, 100),
                                        GiroEmis: creditor.commercial_business.substring(0, 40),
                                        CorreoEmisor: "cobranzas@buildingenergy.it",
                                        Acteco: 401019,
                                        DirOrigen: creditor.commercial_address.substring(0, 60),
                                        CmnaOrigen: "",
                                        CiudadOrigen: ""
                                    },
                                    Receptor: {
                                        RUTRecep: numberWithThousands(debtor.rut) + "-" + debtor.verification_code,
                                        RznSocRecep: debtor.business_name.substring(0, 100),
                                        GiroRecep: debtor.commercial_business.substring(0, 40),
                                        Contacto: "",
                                        DirRecep: (info_place.route) ? info_place.route + " N° " + info_place.street_number : debtor.commercial_address.substring(0, 60),
                                        CmnaRecep: (info_place.locality) ? info_place.locality : "",
                                        CiudadRecep: (info_place.city) ? info_place.city : ""
                                    },
                                    Totales: {
                                        MntNeto: instruction.amount,
                                        MntExe: 0,
                                        TasaIVA: 19,
                                        IVA: Math.round(instruction.amount * 1.19) - instruction.amount,
                                        MntTotal: Math.round(instruction.amount * 1.19)
                                    }
                                },
                                Detalle: {
                                    NroLinDet: 1,
                                    CdgItem: {
                                        TpoCodigo: "CFN",
                                        VlrCodigo: "4-1-1-01-01"
                                    },
                                    NmbItem: payment_matrix.natural_key,
                                    DscItem: payment_matrix.billingWindow.billingType.title + " (Carta " + payment_matrix.letter_code + ", balance " + payment_matrix.billingWindow.periods + ")",
                                    QtyItem: 1,
                                    UnmdItem: "UN",
                                    PrcItem: instruction.amount,
                                    MontoItem: instruction.amount
                                },
                                Referencia: [{
                                    NroLinRef: 1,
                                    TpoDocRef: "SEN",
                                    FolioRef: payment_matrix.reference_code,
                                    FchRef: payment_matrix.publish_date,
                                    RazonRef: payment_matrix.natural_key
                                },
                                {
                                    NroLinRef: 2,
                                    TpoDocRef: "CEC",
                                    FolioRef: debtor.code_for_fact,
                                    FchRef: payment_matrix.publish_date
                                }]
                            }
                        }
                    }
                } else {

                    var data = {
                        _declaration: {
                            _attributes: { version: "1.0", encoding: "ISO-8859-1" }
                        },
                        DTE: {
                            _attributes: { version: "1.0" },
                            Documento: {
                                _attributes: { ID: "F" + folio + "T33" },
                                Encabezado: {
                                    /*
                                    IdDoc: {
                                        TipoDTE: 33,
                                        Folio: folio,
                                        FchEmis: moment("2019-03-30").format("YYYY-MM-DD"), //moment().format("YYYY-MM-DD"),
                                        FmaPago: 2,
                                        TermPagoGlosa: "CREDITO SII",
                                        FchVenc: moment("2019-03-30").add(8, "day").format("YYYY-MM-DD")
                                    },*/
                                    IdDoc: {
                                        TipoDTE: 33,
                                        Folio: folio,
                                        FchEmis: moment(fechaEmis).format("YYYY-MM-DD"),
                                        FmaPago: 2,
                                        TermPagoGlosa: "CREDITO SII",
                                        FchVenc: moment(fechaEmis).add(8, "day").format("YYYY-MM-DD")
                                    },
                                    Emisor: {
                                        RUTEmisor: numberWithThousands(creditor.rut) + "-" + creditor.verification_code,
                                        RznSoc: creditor.business_name.substring(0, 100),
                                        GiroEmis: creditor.commercial_business.substring(0, 40),
                                        CorreoEmisor: "cobranzas@buildingenergy.it",
                                        Acteco: 401019,
                                        DirOrigen: creditor.commercial_address.substring(0, 60),
                                        CmnaOrigen: "",
                                        CiudadOrigen: ""
                                    },
                                    Receptor: {
                                        RUTRecep: numberWithThousands(debtor.rut) + "-" + debtor.verification_code,
                                        RznSocRecep: debtor.business_name.substring(0, 100),
                                        GiroRecep: debtor.commercial_business.substring(0, 40),
                                        Contacto: "",
                                        DirRecep: (info_place.route) ? info_place.route + " N° " + info_place.street_number : debtor.commercial_address.substring(0, 60),
                                        CmnaRecep: (info_place.locality) ? info_place.locality : "",
                                        CiudadRecep: (info_place.city) ? info_place.city : ""
                                    },
                                    Totales: {
                                        MntNeto: instruction.amount,
                                        MntExe: 0,
                                        TasaIVA: 19,
                                        IVA: Math.round(instruction.amount * 1.19) - instruction.amount,
                                        MntTotal: Math.round(instruction.amount * 1.19)
                                    }
                                },
                                Detalle: {
                                    NroLinDet: 1,
                                    CdgItem: {
                                        TpoCodigo: "CFN",
                                        VlrCodigo: "4-1-1-01-01"
                                    },
                                    NmbItem: payment_matrix.natural_key,
                                    DscItem: payment_matrix.billingWindow.billingType.title + " (Carta " + payment_matrix.letter_code + ", balance " + payment_matrix.billingWindow.periods + ")",
                                    QtyItem: 1,
                                    UnmdItem: "UN",
                                    PrcItem: instruction.amount,
                                    MontoItem: instruction.amount
                                },
                                Referencia: {
                                    NroLinRef: 1,
                                    TpoDocRef: "SEN",
                                    FolioRef: payment_matrix.reference_code,
                                    FchRef: payment_matrix.publish_date,
                                    RazonRef: payment_matrix.natural_key
                                }
                            }
                        }
                    }
                }

                // CONVERT JS OBJECT TO XML FILE AND ENCODE IT IN 64BIT FOR USE IN SOAP REQ
                var json2xml = convertXML.js2xml(data, { compact: true, spaces: 4 });
                var xmlEncoded64 = Buffer.from(json2xml).toString('base64');

                // SAVE THE XML IN A LOCAL FOLDER 
                var xml = require('fs').writeFileSync(path.join(global.appRoot, '/public/invoice/xml/F' + folio + 'T33.xml'), json2xml, 'utf8');

                getLogin(instruction.creditor, function (err, result) {
                    facturacion_cl.loadInvoice(instruction, xmlEncoded64, "2", result, function (err, result) {
                        if (err) {
                            logger.log(err);
                            return callback(err, result);
                        }
                        callback(null, result);
                    });
                });
            });
        });
    }

    loadInvoice(instruction, xml, type, login, callback) {
        var args = {};

        logger.log("Connect to ..." + this.endpoint_local);

        var options = {
            wsdl_options: { timeout: 10000 }
        };

        soap.createClient(this.endpoint_local, options, function (err, client) {

            if (err) {
                return callback(err, false);
            }

            logger.log("Connected to " + facturacion_cl.endpoint_local);

            facturacion_cl.login = {
                Usuario: Buffer.from(login.username_fact).toString('base64'),
                Rut: Buffer.from(login.rut_fact).toString('base64'),
                Clave: Buffer.from(login.password_fact).toString('base64'),
                IncluyeLink: "1",
                Puerto: "9978"
            }

            args = {
                login: facturacion_cl.login,
                file: xml,
                formato: type
            }

            logger.log("Creating Invoice");
            client.Procesar(args, function (err, result, rawResponse, soapHeader, rawRequest) {

                if (err) {
                    logger.log("error");
                    return callback(err, false);
                }

                var resultJs = convertXML.xml2js(result.ProcesarResult, { compact: true, spaces: 3 });
                //console.log(resultJs);

                // IF RESULT / CREATION IS TRUE
                if (resultJs.WSPLANO.Detalle.Documento.Resultado._text === "True") {
                    logger.log("Created Invoice");

                    var data = {
                        instruction: instruction.id_cen,
                        company: instruction.creditor,
                        gross_amount: Math.round(instruction.amount * 1.19),
                        net_amount: instruction.amount,
                        folio: parseInt(resultJs.WSPLANO.Detalle.Documento.Folio._text),
                        type: parseInt(resultJs.WSPLANO.Detalle.Documento.TipoDte._text),
                        operation: resultJs.WSPLANO.Detalle.Documento.Operacion._text,
                        urlCedible: Buffer.from(resultJs.WSPLANO.Detalle.Documento.urlCedible._text, 'base64').toString('ascii'),
                        urlOriginal: Buffer.from(resultJs.WSPLANO.Detalle.Documento.urlOriginal._text, 'base64').toString('ascii'),
                        xml: Buffer.from(xml, 'base64').toString('ascii'),
                        created_at: new Date(resultJs.WSPLANO.Detalle.Documento.Fecha._text),
                        reported_by_creditor: true
                    }
                } else {
                    logger.log("Invoice not created for some errors");
                    //logger.log(resultJs.WSPLANO.Detalle.Documento.Error._text);
                    var data = {
                        instruction: instruction.id_cen,
                        company: instruction.creditor,
                        gross_amount: Math.round(instruction.amount * 1.19),
                        net_amount: instruction.amount,
                        error: resultJs.WSPLANO.Detalle.Documento.Error._text,
                        folio: parseInt(resultJs.WSPLANO.Detalle.Documento.Folio._text),
                        type: parseInt(resultJs.WSPLANO.Detalle.Documento.TipoDte._text),
                        operation: resultJs.WSPLANO.Detalle.Documento.Operacion._text,
                        xml: Buffer.from(xml, 'base64').toString('ascii'),
                        created_at: new Date(resultJs.WSPLANO.Detalle.Documento.Fecha._text),
                        reported_by_creditor: true
                    }
                }

                if (data.error) {

                    models.dte_info.findOrCreate({ where: { folio: data.folio } })
                        .spread((record, created) => {
                            record.update(data);
                            return callback(data.error, false);
                        })
                        .catch(function (error) {
                            logger.log(error);
                            return callback(err, false);
                        });

                } else {

                    logger.log("Loading into CEN");

                    cen.putAuxiliaryFiles(data, function (err, invoice_file_id, file_url) {
                        logger.log("Loaded into CEN");
                        if (err) return callback(err, false);
                        data['invoice_file_id_cen'] = invoice_file_id;
                        data['file_url_cen'] = file_url;

                        models.dte_info.findOrCreate({ where: { folio: data.folio } })
                            .spread((record, created) => {
                                record.update(data);

                                cen.postCreateDte(data, function (err, result) {
                                    if (err) return callback(err, false);
                                    return callback(null, true);
                                });
                            })
                            .catch(function (error) {
                                logger.log(error);
                                return callback(err, false);
                            });

                    });
                }

            });
        });
    }

    loadNota(instruction, dte, xml, type, login, callback) {
        var args = {};

        logger.log("Connect to ..." + this.endpoint_local);

        var options = {
            wsdl_options: { timeout: 10000 }
        };

        soap.createClient(this.endpoint_local, options, function (err, client) {

            if (err) {
                return callback(err, false);
            }

            logger.log("Connected to " + facturacion_cl.endpoint_local);

            facturacion_cl.login = {
                Usuario: Buffer.from(login.username_fact).toString('base64'),
                Rut: Buffer.from(login.rut_fact).toString('base64'),
                Clave: Buffer.from(login.password_fact).toString('base64'),
                IncluyeLink: "1",
                Puerto: "9978"
            }

            args = {
                login: facturacion_cl.login,
                file: xml,
                formato: type
            }

            logger.log("Creating Nota");
            client.Procesar(args, function (err, result, rawResponse, soapHeader, rawRequest) {

                if (err) {
                    logger.log("error");
                    return callback(err, false);
                }

                var resultJs = convertXML.xml2js(result.ProcesarResult, { compact: true, spaces: 3 });
                //console.log(resultJs);

                // IF RESULT / CREATION IS TRUE
                if (resultJs.WSPLANO.Detalle.Documento.Resultado._text === "True") {
                    logger.log("Created Nota");

                    var data = {
                        instruction: instruction.id_cen,
                        company: instruction.creditor,
                        gross_amount: Math.round(instruction.amount * 1.19),
                        net_amount: instruction.amount,
                        folio: parseInt(resultJs.WSPLANO.Detalle.Documento.Folio._text),
                        type: parseInt(resultJs.WSPLANO.Detalle.Documento.TipoDte._text),
                        operation: resultJs.WSPLANO.Detalle.Documento.Operacion._text,
                        //urlCedible: Buffer.from(resultJs.WSPLANO.Detalle.Documento.urlCedible._text, 'base64').toString('ascii'),
                        urlOriginal: Buffer.from(resultJs.WSPLANO.Detalle.Documento.urlOriginal._text, 'base64').toString('ascii'),
                        xml: Buffer.from(xml, 'base64').toString('ascii'),
                        created_at: new Date(resultJs.WSPLANO.Detalle.Documento.Fecha._text),
                        reported_by_creditor: true
                    }
                } else {
                    logger.log("Nota not created for some errors");
                    //logger.log(resultJs.WSPLANO.Detalle.Documento.Error._text);
                    var data = {
                        instruction: instruction.id_cen,
                        company: instruction.creditor,
                        gross_amount: Math.round(instruction.amount * 1.19),
                        net_amount: instruction.amount,
                        error: resultJs.WSPLANO.Detalle.Documento.Error._text,
                        folio: parseInt(resultJs.WSPLANO.Detalle.Documento.Folio._text),
                        type: parseInt(resultJs.WSPLANO.Detalle.Documento.TipoDte._text),
                        operation: resultJs.WSPLANO.Detalle.Documento.Operacion._text,
                        xml: Buffer.from(xml, 'base64').toString('ascii'),
                        created_at: new Date(resultJs.WSPLANO.Detalle.Documento.Fecha._text),
                        reported_by_creditor: true
                    }
                }

                if (data.error) {

                    models.nota_info.findOrCreate({ where: { folio: data.folio } })
                        .spread((record, created) => {
                            record.update(data);
                            return callback(data.error, false);
                        })
                        .catch(function (error) {
                            logger.log(error);
                            return callback(err, false);
                        });

                } else {

                    cen.postDeleteDte(dte.id_cen, function (err, result) {
                        if (err) {
                            console.log(err);
                        }

                        models.nota_info.findOrCreate({ where: { folio: data.folio } })
                            .spread((record, created) => {
                                record.update(data);
                                return callback(null, true);
                            })
                            .catch(function (error) {
                                logger.log(error);
                                return callback(err, false);
                            });

                    });


                }

            });
        });
    }

}



async function getLastFolio(id, callback) {
    var folioNew;

    await models.dte_info.findAll({
        where: {
            company: id
        },
        limit: 1,
        order: [['folio', 'DESC']]
    }).then(function (result) {
        if (result.length === undefined || result.length === 0) {
            folioNew = 1;
        } else {
            folioNew = result[0].folio + 1;
        }
    });

    await models.dte_info.findOrCreate({ where: { folio: folioNew } })
        .spread((record, created) => {
            return callback(null, folioNew);
        })
        .catch(function (error) {
            logger.log(error);
            return callback(error, false);
        });

}


async function getLastNota(id, callback) {
    var folioNew;

    await models.nota_info.findAll({
        where: {
            company: id
        },
        limit: 1,
        order: [['folio', 'DESC']]
    }).then(function (result) {
        if (result.length === undefined || result.length === 0) {
            folioNew = 1;
        } else {
            folioNew = result[0].folio + 1;
        }
    });

    await models.nota_info.findOrCreate({ where: { folio: folioNew } })
        .spread((record, created) => {
            return callback(null, folioNew);
        })
        .catch(function (error) {
            logger.log(error);
            return callback(error, false);
        });

}

async function getLogin(id, callback) {

    await models.plants.findAll({
        where: {
            company_cen_id: id
        },
        limit: 1
    }).then(function (result) {
        if (result.length === undefined) {
            return callback(err, "No plants");
        } else {
            return callback(null, result[0]);
        }
    });

}

function numberWithThousands(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

module.exports = FacturacionCL;
