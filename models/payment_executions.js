/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  const payment_executions = sequelize.define('payment_executions', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    id_cen: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    instruction: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    execution: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    amount: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    is_net_amount: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    dtes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_ts: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_ts: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
      tableName: 'payment_executions',
      indexes: [
        // Create a index
        {
          unique: false,
          fields: ['id_cen']
        }
      ]
    });

  payment_executions.associate = (models) => {
    // associations can be defined here
    /*
    instructions.belongsTo(models.company, { foreignKey: "creditor", as: "creditor_info" });
    instructions.belongsTo(models.company, { foreignKey: "debtor", as: "debtor_info" });*/
  };

  return payment_executions;

};
